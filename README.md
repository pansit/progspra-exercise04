# ProgSpra-exercise04
## Interessante Anwendungsfälle für [`std::accumulate`](http://en.cppreference.com/w/cpp/algorithm/accumulate)
Der STL-Algorithmus [`std::accumulate`](http://en.cppreference.com/w/cpp/algorithm/accumulate) kann nicht nur zum Aufsummieren verwendet werden. Die Template-Funktion verfügt über zwei Varianten.

### Erste Variante
```cpp
template<class InputIt, class T>
T accumulate(InputIt first, InputIt last, T init) {
    for (; first != last; ++first) {
        init = init + *first;
    }
    return init;
}
```
Diese Variante bekommt Parameter übergeben. Die ersten beiden sind Iteratoren eines Bereichs. Der dritte ist ein Initialwert auf den aufsummiert wird. Der Algorithmus iteriert über den Bereich und addiert jedes Element auf den Initialwert auf.

### Zweite Variante
```cpp
template<class InputIt, class T, class BinaryOperation>
T accumulate(InputIt first, InputIt last, T init, BinaryOperation op) {
    for (; first != last; ++first) {
        init = op(init, *first);
    }
    return init;
}
```
Die zweite Variante von [`std::accumulate`](http://en.cppreference.com/w/cpp/algorithm/accumulate) kann mit dem vierten Parameter individuell angepasst werden. Hier erwartet die Funktion eine binäre Operation, die ein Objekt vom Typ `T` zurückgibt. Anstelle einer Ausfummierung findet dann eine individuelle Operation statt.