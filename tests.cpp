#define CATCH_CONFIG_RUNNER
#include "catch.hpp"
#include "circle.hpp"
#include "mylib.hpp"

#include <algorithm>
#include <iostream>
#include <iterator>
#include <list>
#include <numeric>

TEST_CASE("AUFGABE 1 - Sort mit Lambda", "[sort_circle]")
{
  /**
   * ---------------------------------------------------------------------------
   * ---------------------------------------------------------------------------
   * Objekte der Klasse circle sollen in einem STL-Container gespeichert und der
   * Radiusgröße nach sortiert werden. Diesmal soll allerdings std::sort die
   * Vergleichsfunktion als Parameter übergeben werden. Implementieren Sie die
   * Vergleichsfunktion mit Hilfe eines Lambdas. Testen Sie danach mit der
   * Funktion std::is_sorted ob der Container sortiert ist.
   *
   * REQUIRE(std::is_sorted(container.begin(), container.end()));
   *
   *                                                                  [5 Punkte]
   * ---------------------------------------------------------------------------
   */
  // std::function<bool(Circle const&,Circle const&)>
  auto lambda = [](Circle const& lhs, Circle const& rhs){
    return lhs < rhs;
  };

  std::list<Circle> l0(50);
  std::generate(std::begin(l0), std::end(l0), ren::generate<Circle, 50>);
  l0.sort(lambda);

  REQUIRE(std::is_sorted(std::begin(l0), std::end(l0)));
}

TEST_CASE("AUFGABE 1 - Generate mit Funktor", "[sort_circle]")
{
  /**
   * ---------------------------------------------------------------------------
   * ---------------------------------------------------------------------------
   * Füllen der Liste mit std::generate und einem eigenen Funktor ren::Spawn.
   *                                                                  [0 Punkte]
   * ---------------------------------------------------------------------------
   */
  std::list<Circle> l0(50);
  std::generate(std::begin(l0), std::end(l0), ren::Spawn<Circle, 50>());

  l0.sort(ren::greater<Circle>());
  REQUIRE(std::is_sorted(l0.rbegin(), l0.rend()));
}

TEST_CASE("AUFGABE 2 - Sort mit Funktor", "[sort_circle]")
{
  /**
   * ---------------------------------------------------------------------------
   * ---------------------------------------------------------------------------
   * Lösen Sie Aufgabe 4.1 unter Verwendung eines Funktors.
   *                                                                  [5 Punkte]
   * ---------------------------------------------------------------------------
   */
  struct {
    bool operator() (Circle const& a, Circle const& b) const {
      return a < b;
    }
  } less;

  std::list<Circle> l0(50);
  std::generate(std::begin(l0), std::end(l0), ren::generate<Circle, 50>);
  l0.sort(less);

  REQUIRE(std::is_sorted(std::begin(l0), std::end(l0)));
}

TEST_CASE("AUFGABE 3 - Add with std::transform", "[transform]")
{
  /**
   * ---------------------------------------------------------------------------
   * ---------------------------------------------------------------------------
   * Addieren Sie die gegeben Container v1 und v2 elementweise auf und speichern
   * Sie das Ergebnis im Container v3. Verwenden Sie dafür den Algorithmus
   * std::transform und ein Lambda.
   *
   * std::vector<int> v1{1,2,3,4,5};
   * std::vector<int> v2{9,8,7,6,5};
   * std::vector<int> v3(5);
   *
   * Testen Sie danach mit REQUIRE(std::all_of()), dass die Elemente in v3 alle
   * gleich zehn sind unter Verwendung eines Lambdas.
   *                                                                  [5 Punkte]
   * ---------------------------------------------------------------------------
   */
  std::vector<int> v1{1,2,3,4,5};
  std::vector<int> v2{9,8,7,6,5};
  std::vector<int> v3(5);

  // std::function<int(int const,int const)>
  auto lambda = [](int const lhs, int const rhs){
    return lhs + rhs;
  };

  std::transform(
      std::begin(v1), std::end(v1), std::begin(v2), std::begin(v3), lambda);

  REQUIRE(std::all_of(std::begin(v3), std::end(v3),
      [](int const i){ return i == 10; }));
}

TEST_CASE("AUFGABE 4 - std::copy_if", "[copy_if]")
{
  /**
   * ---------------------------------------------------------------------------
   * ---------------------------------------------------------------------------
   * Legen Sie einen std::vector mit Objekten der Klasse circle an. Alle Kreise
   * sollen verschiedene Radien haben. Zum Beispiel:
   *
   * Vorrausgesetzt es gibt einen Konstruktor der einen Radius als Parameter
   * bekommt.
   * std::vector<circle> circles{{4.0f}, {3.0f}, {8.0f}, {1.0f}, {5.0f}};
   *
   * Kopieren Sie anschliessend mit dem Algorithmus copy_if alle Kreise deren
   * Radius größer als 3.0 ist, in einen zweiten std::vector. Verwenden Sie für
   * das benötigte Prädikat wieder ein Lambda. Testen Sie danach mit
   * std::all_of, dass die Radien im Zielcontainer alle größer drei sind (unter
   * Verwendung eines Lambdas).
   *                                                                  [5 Punkte]
   * ---------------------------------------------------------------------------
   */
  std::vector<Circle> v0(20);
  std::vector<Circle> v1(v0.size());

  // std::function<bool(Circle const&)>
  auto lambda = [](Circle const& c){ return c > 3; };
  struct {
    bool operator () (Circle const& c) {
      return c > 3;
    }
  } functor;

  std::generate(std::begin(v0), std::end(v0), ren::generate<Circle, 10>);
  auto it = std::copy_if(std::begin(v0), std::end(v0), std::begin(v1), lambda);

  v1.resize(std::distance(std::begin(v1), it));
  std::copy(v1.begin(), v1.end(), std::ostream_iterator<Circle>(std::cout, "\n"));
  REQUIRE(std::all_of(std::begin(v1), std::end(v1), functor));
}

TEST_CASE("AUFGABE 5 - std::accumulate", "[accumulate]")
{
  /**
   * ---------------------------------------------------------------------------
   * ---------------------------------------------------------------------------
   * Diese Aufgabe beschäftigt sich mit der Funktion std::accumulate aus dem
   * Header <numeric>. Erklären Sie die beiden Varianten des Algorithmus
   * (http://en.cppreference.com/w/cpp/algorithm/accumulate). Implementieren Sie
   * eine Klasse Vec3 mit folgendem Interface:
   *
   * struct Vec3 {
   *   double x;
   *   double y;
   *   double z;
   *   friend Vec3 operator+(Vec3 const& a, Vec3 const& b);
   *   friend bool operator==(Vec3 const& a, Vec3 const& b);
   * };
   *
   * Legen Sie sich in einem Test einen std::vector<Vec3> mit mindestens 10 Vec3
   * an. Diese können Sie mit std::accumulate aufaddieren:
   *
   * auto sum1 = std::accumulate(vs.begin(), vs.end(), Vec3{0.0,0.0,0.0});
   *
   * Einer zweiten Variante von std::accumulate kann als viertes Argument eine
   * Funktion übergeben werden. Verwenden Sie einmal std::plus aus dem Header
   * functional und einmal ein selbstgeschriebenes Lambda um die Vektoren
   * aufzuaddieren.
   *
   * With std::plus:
   * auto sum2 = std::accumulate(vs.begin(), vs.end(), Vec3{0.0,0.0,0.0}, [op]);
   *
   * With Lambda:
   * auto sum3 = std::accumulate(vs.begin(), vs.end(), Vec3{0.0,0.0,0.0}, [op]);
   *
   * Testen Sie mit REQUIRE das alle drei Summen gleich sind.
   *                                                                 [10 Punkte]
   * ---------------------------------------------------------------------------
   */
  std::vector<ren::Vec3> v(10);
  std::generate(std::begin(v), std::end(v), [](){ return ren::Vec3{2,2,2}; });

  auto sum1 = std::accumulate(std::begin(v), std::end(v), ren::Vec3{0,0,0});

  auto sum2 = std::accumulate(std::begin(v), std::end(v), ren::Vec3{0,0,0},
      std::plus<ren::Vec3>());

  // std::function<ren::Vec3(ren::Vec3 const&,ren::Vec3 const&)>
  auto lambda = [](ren::Vec3 const& a, ren::Vec3 const& b){
    return a + b;
  };

  auto sum3 = std::accumulate(std::begin(v), std::end(v), ren::Vec3{0,0,0},
      lambda);

  ren::Vec3 res{20,20,20};
  REQUIRE(sum1 == res);
  REQUIRE(sum2 == res);
  REQUIRE(sum3 == res);
}

TEST_CASE("AUFGABE 5 (ALT) - std::accumulate", "[accumulate]")
{
  /**
   * ---------------------------------------------------------------------------
   * ---------------------------------------------------------------------------
   * Diese Aufgabe beschäftigt sich mit der Funktion std::accumulate aus dem
   * Header <algorithm>. Legen Sie einen Vektor von Studenten wie im Beispiel
   * angegeben an.
   *
   * typedef int matrikel;
   * std::vector<std::pair<matrikel,std::string>> v{
   *   {1, "Alonzo Church"},
   *   {3, "Haskell Curry"},
   *   {4, "Moses Schoenfinkel"}
   * };
   *
   * Verwenden Sie nun std::accumulate um die Studenten in eine
   * std::map<matrikel, std::string> einzutragen. Diese map wird als 3. Pa-
   * rameter an accumulate übergeben. Als vierten Parameter übergeben Sie ein
   * selbstgeschriebenes Lambda. Testen Sie danach, ob alle Studenten in die map
   * eingetragen wurden.
   *                                                                  [5 Punkte]
   * ---------------------------------------------------------------------------
   */
  typedef int matrikel;
  std::vector<std::pair<matrikel, std::string>> v{
    {1, "Alonzo Church"},
    {3, "Haskell Curry"},
    {4, "Moses Schoenfinkel"}
  };
  std::map<matrikel, std::string> m;
  auto l =
      [](std::map<int, std::string>& m, std::pair<int, std::string> const& b) {
        m.insert(b);
        return m;
      };

  m = std::accumulate(std::begin(v), std::end(v), m, l);

  REQUIRE(m.size() == 3);
  REQUIRE(m[1] == "Alonzo Church");
  REQUIRE(m[3] == "Haskell Curry");
  REQUIRE(m[4] == "Moses Schoenfinkel");
}

int main(int argc, char* argv[])
{
  return Catch::Session().run(argc, argv);
}